//
//  LinuxMain.swift
//  VaporTestHelpers
//
//  Created by Gustavo Perdomo on 07/23/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.
//

import XCTest

import VaporTestHelpersTests

var tests = [XCTestCaseEntry]()
tests += VaporTestHelpersTests.allTests()

XCTMain(tests)
