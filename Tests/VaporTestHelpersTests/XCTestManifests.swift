//
//  XCTestManifests.swift
//  VaporTestHelpers
//
//  Created by Gustavo Perdomo on 07/23/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.
//

import XCTest

#if !os(macOS)
    public func allTests() -> [XCTestCaseEntry] {
        return [
            testCase(HTTPHeadersRepresentableTests.allTests)
        ]
    }
#endif
