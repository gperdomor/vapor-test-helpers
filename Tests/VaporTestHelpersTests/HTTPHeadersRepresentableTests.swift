//
//  HTTPHeadersRepresentableTests.swift
//  VaporTestHelpers
//
//  Created by Gustavo Perdomo on 08/20/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.
//

@testable import VaporTestHelpers

import Vapor
import XCTest

final class HTTPHeadersRepresentableTests: XCTestCase {
    func testDictionary() {
        let date = Date()
        let dictionary = [
            "Authorization": "Bearer 12345",
            "Content-Type": "application/json",
            "X-Date": "\(date.description)"
        ]

        let headers = dictionary.convertToHTTPHeaders()

        XCTAssertEqual(headers.firstValue(name: .authorization), "Bearer 12345")
        XCTAssertEqual(headers.firstValue(name: .contentType), "application/json")
        XCTAssertEqual(headers.firstValue(name: .init("X-Date")), date.description)
    }

    func testHTTPHeaders() {
        let date = Date()
        let headers = HTTPHeaders([("Authorization", "Bearer 12345"), ("Content-Type", "application/json"), ("X-Date", "\(date.description)")])

        XCTAssertEqual(headers.firstValue(name: .authorization), "Bearer 12345")
        XCTAssertEqual(headers.firstValue(name: .contentType), "application/json")
        XCTAssertEqual(headers.firstValue(name: .init("X-Date")), date.description)
    }

    /// Check Linux Tests
    func testLinuxTestSuiteIncludesAllTests() throws {
        #if os(macOS) || os(iOS) || os(tvOS) || os(watchOS)
            let thisClass = type(of: self)
            let linuxCount = thisClass.allTests.count
            let darwinCount = Int(thisClass.defaultTestSuite.testCaseCount)

            XCTAssertEqual(linuxCount, darwinCount, "\(darwinCount - linuxCount) tests are missing from allTests")
        #endif
    }

    static var allTests = [
        ("testLinuxTestSuiteIncludesAllTests", testLinuxTestSuiteIncludesAllTests),
        ("testDictionary", testDictionary),
        ("testHTTPHeaders", testHTTPHeaders)
    ]
}
