//
//  ResponseTests.swift
//  VaporTestHelpers
//
//  Created by Gustavo Perdomo on 08/20/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.
//

@testable import VaporTestHelpers

import Vapor
import XCTest

// swiftlint:disable force_try
final class ResponseTests: XCTestCase {
    var app: Application!
    var response: Response!
    let data = "This is a test".convertToData()

    override func setUp() {
        app = try! Application.testable({ _, _, services in
            services.register(ContentConfig.default())
        }, { _ in

        })

        let httpResp = HTTPResponse(status: .badGateway, headers: .init([("Authorization", "Bearer 12345"), ("Content-Type", "application/text")]), body: data)

        response = Response(http: httpResp, using: app)
    }

    func testHas() throws {
        XCTAssertTrue(response.has(header: .authorization))
        XCTAssertTrue(response.has(header: .contentType))

        XCTAssertTrue(response.has(header: .authorization, value: nil))
        XCTAssertTrue(response.has(header: .contentType, value: nil))

        XCTAssertTrue(response.has(header: .authorization, value: "Bearer 12345"))
        XCTAssertTrue(response.has(header: .contentType, value: "application/text"))

        XCTAssertFalse(response.has(header: .eTag))

        XCTAssertFalse(response.has(header: .authorization, value: "Bearer"))
        XCTAssertFalse(response.has(header: .contentType, value: "application/json"))
        XCTAssertFalse(response.has(header: .contentType, value: "application"))
    }

    func testHasContentLength() throws {
        XCTAssertTrue(response.has(contentLength: data.count))
        XCTAssertFalse(response.has(contentLength: 0))

        response.http.body = HTTPBody(data: Data())

        XCTAssertTrue(response.has(contentLength: 0))

        let body = ":D".convertToData()
        response.http.body = HTTPBody(data: body)

        XCTAssertTrue(response.has(contentLength: body.count))
    }

    func testHasContent() throws {
        XCTAssertTrue(response.has(content: "This is a test"))
        XCTAssertFalse(response.has(content: "This"))

        response.http.body = HTTPBody(data: ":D".convertToData())

        XCTAssertTrue(response.has(content: ":D"))
    }

    func testHasStatusMessage() throws {
        XCTAssertTrue(response.has(status: .badGateway))
        XCTAssertTrue(response.has(status: .badGateway, message: nil))
        XCTAssertTrue(response.has(status: .badGateway, message: "Bad Gateway"))

        XCTAssertFalse(response.has(status: .ok))

        response.http.status = .conflict

        XCTAssertTrue(response.has(status: .conflict))
        XCTAssertTrue(response.has(status: .conflict, message: nil))
        XCTAssertTrue(response.has(status: .conflict, message: "Conflict"))
    }

    func testDecoding() throws {
        let data = try JSONEncoder().encode(User(firstName: "Homer", lastName: "Simpson"))
        let http = HTTPResponse(status: .badGateway, headers: .init([("Content-Type", "application/json")]), body: data)
        let response = Response(http: http, using: app)

        var user = try response.decode(as: User.self)

        XCTAssertEqual(user.firstName, "Homer")
        XCTAssertEqual(user.lastName, "Simpson")

        user = try response.decode(as: User.self, using: JSONDecoder())

        XCTAssertEqual(user.firstName, "Homer")
        XCTAssertEqual(user.lastName, "Simpson")
    }

    func testGetters() throws {
        XCTAssertEqual(response.header(name: "Authorization"), "Bearer 12345")
        XCTAssertEqual(response.header(name: .authorization), "Bearer 12345")
        XCTAssertNil(response.header(name: .eTag))

        XCTAssertEqual(response.contentSize, data.count)
        XCTAssertEqual(response.contentString(), "This is a test")

        response.http.body = HTTPBody()

        XCTAssertEqual(response.contentSize, 0)
        XCTAssertEqual(response.contentString(), "")
    }

    /// Check Linux Tests
    func testLinuxTestSuiteIncludesAllTests() throws {
        #if os(macOS) || os(iOS) || os(tvOS) || os(watchOS)
            let thisClass = type(of: self)
            let linuxCount = thisClass.allTests.count
            let darwinCount = Int(thisClass.defaultTestSuite.testCaseCount)

            XCTAssertEqual(linuxCount, darwinCount, "\(darwinCount - linuxCount) tests are missing from allTests")
        #endif
    }

    static var allTests = [
        ("testLinuxTestSuiteIncludesAllTests", testLinuxTestSuiteIncludesAllTests),
        ("testHas", testHas),
        ("testHasContentLength", testHasContentLength),
        ("testHasContent", testHasContent),
        ("testHasStatusMessage", testHasStatusMessage),
        ("testDecoding", testDecoding),
        ("testGetters", testGetters)
    ]
}

struct User: Codable {
    var firstName: String
    var lastName: String
}
