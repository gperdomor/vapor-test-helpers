// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "VaporTestHelpers",
    products: [
        .library(name: "VaporTestHelpers", targets: ["VaporTestHelpers"]),
    ],
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),
    ],
    targets: [
        .target(name: "VaporTestHelpers", dependencies: ["Vapor"]),
        .testTarget(name: "VaporTestHelpersTests", dependencies: ["VaporTestHelpers"]),
    ]
)
