//
//  HTTPHeadersRepresentable.swift
//  VaporTestHelpers
//
//  Created by Gustavo Perdomo on 08/13/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.
//

import Vapor

public protocol HTTPHeadersRepresentable {
    /// Converts `Self` to a `HTTPHeaders`, returning `nil` if the conversion fails.
    func convertToHTTPHeaders() -> HTTPHeaders
}

extension HTTPHeaders: HTTPHeadersRepresentable {
    /// See `URLRepresentable`.
    public func convertToHTTPHeaders() -> HTTPHeaders {
        return self
    }
}

extension Dictionary: HTTPHeadersRepresentable where Key: StringProtocol, Value: StringProtocol {
    /// See `convertToHTTPHeaders`.
    public func convertToHTTPHeaders() -> HTTPHeaders {
        var headers = HTTPHeaders()
        for (key, value) in self {
            headers.replaceOrAdd(name: key.description, value: value.description)
        }

        return headers
    }
}
