//
//  Application+Testable.swift
//  VaporTestHelpers
//
//  Created by Gustavo Perdomo on 07/23/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.
//

import Vapor

public extension Application {
    public typealias AppConfigClosure = (_ config: inout Config, _ env: inout Vapor.Environment, _ services: inout Services) throws -> Void
    public typealias AppBootClosure = (_ app: Application) throws -> Void

    static func testable(envArgs: [String]? = nil, _ configureClosure: AppConfigClosure, _ bootClosure: AppBootClosure) throws -> Application {
        var config = Config.default()
        var services = Services.default()
        var env = Environment.testing

        if let environmentArgs = envArgs {
            env.arguments = environmentArgs
        }

        try configureClosure(&config, &env, &services)
        let app = try Application(config: config, environment: env, services: services)

        try bootClosure(app)
        return app
    }

    private func buildRequest(method: HTTPMethod, url: URLRepresentable, version: HTTPVersion? = nil, headers: HTTPHeadersRepresentable? = nil, body: LosslessHTTPBodyRepresentable? = nil) -> HTTPRequest {
        var request = HTTPRequest(method: method, url: url)

        if let version = version {
            request.version = version
        }

        if let headers = headers {
            request.headers = headers.convertToHTTPHeaders()
        }

        if let body = body {
            request.body = body.convertToHTTPBody()
        }

        return request
    }

    /// Respond to a request.
    ///
    /// - Parameter request: The request to process.
    /// - Returns: A Response.
    /// - Throws: Any Error.
    public func respond(to request: HTTPRequest) throws -> Response {
        let responder = try make(Responder.self)
        let wrappedRequest = Request(http: request, using: self)
        return try responder.respond(to: wrappedRequest).wait()
    }

    /// Respond to a request using GET method.
    ///
    /// - Parameters:
    ///   - url: The requested url.
    ///   - version: HTTPVersion.
    ///   - headers: HTTPHeaders.
    /// - Returns: A Response.
    /// - Throws: Any Error.
    public func get(url: URLRepresentable, version: HTTPVersion? = nil, headers: HTTPHeadersRepresentable? = nil) throws -> Response {
        let request = buildRequest(method: .GET, url: url, version: version, headers: headers)
        return try respond(to: request)
    }

    /// Respond to a request using POST method.
    ///
    /// - Parameters:
    ///   - url: The requested url.
    ///   - version: HTTPVersion.
    ///   - headers: HTTPHeaders.
    /// - Returns: A Response.
    /// - Throws: Any Error.
    public func post<U>(url: URLRepresentable, version: HTTPVersion? = nil, headers: HTTPHeadersRepresentable? = nil, body: U? = nil) throws -> Response where U: Encodable {
        var encodedBody: Data?

        if let body = body {
            let coders = try make(ContentCoders.self)
            let jsonEncoder = try coders.requireDataEncoder(for: .json)

            encodedBody = try jsonEncoder.encode(body)
        }

        return try post(url: url, version: version, headers: headers, data: encodedBody)
    }

    public func post(url: URLRepresentable, version: HTTPVersion? = nil, headers: HTTPHeadersRepresentable? = nil, data: Data? = nil) throws -> Response {
        let request = buildRequest(method: .POST, url: url, version: version, headers: headers, body: data)
        return try respond(to: request)
    }

    /// Respond to a request using PUT method.
    ///
    /// - Parameters:
    ///   - url: The requested url.
    ///   - version: HTTPVersion
    ///   - headers: HTTPHeaders
    ///   - body: The encodable body payload.
    /// - Returns: A Response.
    /// - Throws: Any Error.
    public func put<U>(url: URLRepresentable, version: HTTPVersion? = nil, headers: HTTPHeadersRepresentable? = nil, body: U?) throws -> Response where U: Encodable {
        var encodedBody: Data?

        if let body = body {
            let coders = try make(ContentCoders.self)
            let jsonEncoder = try coders.requireDataEncoder(for: .json)

            encodedBody = try jsonEncoder.encode(body)
        }

        return try put(url: url, version: version, headers: headers, data: encodedBody)
    }

    public func put(url: URLRepresentable, version: HTTPVersion? = nil, headers: HTTPHeadersRepresentable? = nil, data: Data? = nil) throws -> Response {
        let request = buildRequest(method: .PUT, url: url, version: version, headers: headers, body: data)
        return try respond(to: request)
    }

    /// Respond to a request using PATCH method.
    ///
    /// - Parameters:
    ///   - url: The requested url.
    ///   - version: HTTPVersion
    ///   - headers: HTTPHeaders
    ///   - body: The encodable body payload.
    /// - Returns: A Response.
    /// - Throws: Any Error.
    public func patch<U>(url: URLRepresentable, version: HTTPVersion? = nil, headers: HTTPHeadersRepresentable? = nil, body: U?) throws -> Response where U: Encodable {
        var encodedBody: Data?

        if let body = body {
            let coders = try make(ContentCoders.self)
            let jsonEncoder = try coders.requireDataEncoder(for: .json)

            encodedBody = try jsonEncoder.encode(body)
        }

        return try patch(url: url, version: version, headers: headers, data: encodedBody)
    }

    public func patch(url: URLRepresentable, version: HTTPVersion? = nil, headers: HTTPHeadersRepresentable? = nil, data: Data? = nil) throws -> Response {
        let request = buildRequest(method: .PATCH, url: url, version: version, headers: headers, body: data)
        return try respond(to: request)
    }

    /// Respond to a request using DELETE method.
    ///
    /// - Parameters:
    ///   - url: The requested url.
    ///   - version: HTTPVersion.
    ///   - headers: HTTPHeaders.
    /// - Returns: A Response.
    /// - Throws: Any Error.
    public func delete(url: URLRepresentable, version: HTTPVersion? = nil, headers: HTTPHeadersRepresentable? = nil) throws -> Response {
        let request = buildRequest(method: .DELETE, url: url, version: version, headers: headers)
        return try respond(to: request)
    }
}
