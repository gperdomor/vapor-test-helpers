//
//  MockLogger.swift
//  VaporTestHelpers
//
//  Created by Gustavo Perdomo on 07/24/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.
//

import Vapor

public struct MockLogger: Logger, Service {
    public func log(_: String, at _: LogLevel, file _: String, function _: String, line _: UInt, column _: UInt) {}
}
