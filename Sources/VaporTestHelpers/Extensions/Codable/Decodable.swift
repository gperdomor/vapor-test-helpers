//
//  Decodable.swift
//  VaporTestHelpers
//
//  Created by Gustavo Perdomo on 08/13/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.
//

import Vapor

extension Decodable {
    public static func fromJSON(filename: String, of type: String? = nil, in bundle: Bundle? = nil, on container: Container) throws -> Self {
        let bdl = bundle ?? Bundle.main

        guard let filePath = bdl.path(forResource: filename, ofType: type) else {
            fatalError("Invalid filename")
        }

        let url = URL(fileURLWithPath: filePath)

        return try from(file: url, on: container)
    }

    /// Decode data from JSON source
    public static func from(file url: URL, on container: Container) throws -> Self {
        let data = try Data(contentsOf: url)
        return try from(data: data, on: container)
    }

    /// Decode data from JSON source
    public static func from(string: String, on container: Container) throws -> Self {
        guard let data = string.data(using: .utf8) else {
            fatalError("Invalid string")
        }
        return try from(data: data, on: container)
    }

    /// Decode data from JSON source
    public static func from(data: Data, on container: Container) throws -> Self {
        let coders = try container.make(ContentCoders.self)
        let decoder = try coders.requireDataDecoder(for: .json)

        return try decoder.decode(Self.self, from: data)
    }
}
