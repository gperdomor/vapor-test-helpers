//
//  Encodable.swift
//  VaporTestHelpers
//
//  Created by Gustavo Perdomo on 08/13/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.
//

import Vapor

extension Encodable {
    /// Convert to Data
    public func data(on container: Container) throws -> Data {
        let coders = try container.make(ContentCoders.self)
        let encoder = try coders.requireDataEncoder(for: .json)

        return try encoder.encode(self)
    }
}
