//
//  Response+Getters.swift
//  VaporTestHelpers
//
//  Created by Gustavo Perdomo on 08/13/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.
//

import Vapor

extension Response {
    /// Get header by it's name
    public func header(name: String) -> String? {
        let headerName = HTTPHeaderName(name)
        return header(name: headerName)
    }

    /// Get header by it's HTTPHeaderName representation
    public func header(name: HTTPHeaderName) -> String? {
        return http.headers[name].first
    }

    /// Size of the content
    public var contentSize: Int? {
        return http.body.count
    }

    /// Get content string with encoding. Maximum of 0.5Mb of text will be returned
    public func contentString(encoding: String.Encoding = .utf8) -> String? {
        guard let data = try? self.http.body.consumeData(max: 500_000, on: self).wait() else {
            return nil
        }
        return String(data: data, encoding: encoding)
    }
}
