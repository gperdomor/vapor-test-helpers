//
//  Response+Decoding.swift
//  VaporTestHelpers
//
//  Created by Gustavo Perdomo on 08/13/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.
//

import Foundation
import Vapor

extension Response {
    /// Decode returned content
    public func decode<T>(as type: T.Type) throws -> T where T: Decodable {
        return try content.decode(type).wait()
    }

    public func decode<T>(as type: T.Type, using decoder: HTTPMessageDecoder) throws -> T where T: Decodable {
        return try content.decode(type, using: decoder).wait()
    }

    public func decode<T>(as type: T.Type, using decoder: JSONDecoder) throws -> T where T: Decodable {
        return try content.decode(json: type, using: decoder).wait()
    }
}
