//
//  Response+Checks.swift
//  VaporTestHelpers
//
//  Created by Gustavo Perdomo on 07/23/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.
//

import Vapor

extension Response {
    public func has(header name: HTTPHeaderName, value: String? = nil) -> Bool {
        guard let header = self.http.headers[name].first else {
            return false
        }

        if let value = value {
            return header == value
        }

        return true
    }

    /// Test header value
    public func has(header name: String, value: String? = nil) -> Bool {
        let headerName = HTTPHeaderName(name)
        return has(header: headerName, value: value)
    }

    /// Test header Content-Type
    public func has(contentType value: String) -> Bool {
        let headerName = HTTPHeaderName("Content-Type")
        return has(header: headerName, value: value)
    }

    /// Test header Content-Length
    public func has(contentLength value: Int) -> Bool {
        let headerName = HTTPHeaderName("Content-Length")
        return has(header: headerName, value: String(value))
    }

    /// Test response status code and message
    public func has(status value: HTTPStatus, message: String? = nil) -> Bool {
        return http.status.code == value.code && (http.status.reasonPhrase == message || message == nil)
    }

    /// Test response content
    public func has(content value: String) -> Bool {
        guard let data = self.http.body.data, let content = String(data: data, encoding: .utf8) else {
            return false
        }

        return content == value
    }
}
