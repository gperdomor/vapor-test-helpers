//
//  Response+Debug.swift
//  VaporTestHelpers
//
//  Created by Gustavo Perdomo on 07/23/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.
//

import Vapor

public extension Response {
    public func debug() {
        print("##########################################")
        print("########### Debugging response ###########")
        print("##########################################")
        print("#  HTTP [\(http.version.major).\(http.version.minor)] with status code [\(http.status.code)]")
        print("#  Headers:")
        for header in http.headers {
            print("#\t\t\(header.name.description) = \(header.value)")
        }
        print("#  Content:")
        if let size = self.http.body.count {
            print("#\t\tSize: \(String(size))")
        }
        if let mediaType = self.http.contentType {
            print("#\t\tMedia type: \(mediaType.description)")
        }
        if let data = self.http.body.data, let content = String(data: data, encoding: .utf8) {
            print("#\t\tContent:\n\(content)")
        }
        print("##########################################")
    }
}
