//
//  Array.swift
//  VaporTestHelpers
//
//  Created by Gustavo Perdomo on 09/06/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.
//

extension Array {
    public subscript(safe index: Int) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
